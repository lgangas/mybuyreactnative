/** @format */

import {AppRegistry} from 'react-native';
//import App from './App';
import Navigator from './components/screens/Navigator';
import {name as appName} from './app.json';
//import Home from './components/screens/tabComponents';

AppRegistry.registerComponent(appName, () => Navigator);
