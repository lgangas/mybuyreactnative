import React,{Component} from 'react'
import {StyleSheet,View, Text, TextInput, TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'
import Global from '../Global';
const GLOBAL = require('../Global')

export default class Login extends Component {

    constructor(props){
        super(props)
        this.state = {
            numero_telefono : '',
            contrasenia : '',
            localhost : '192.168.1.5'
        }
        this.setCellphone = this.setCellphone.bind(this)
        this.setPassword = this.setPassword.bind(this)
        this.setLocalhost = this.setLocalhost.bind(this)
        this.login = this.login.bind(this)
        this.signin = this.signin.bind(this)
    }

    setCellphone(texto){
        this.setState({
            numero_telefono : texto
        })
    }

    setPassword(texto){
        this.setState({
            contrasenia : texto
        })
    }

    setLocalhost(texto){
        this.setState({
            localhost : texto
        })
    }

    login(numero_telefono,contrasenia,localhost){

        let json = {
            numero_telefono,
            contrasenia
        }
        const url = `http://${localhost}:8080/MyBuy/rest/MyBuyRest/ingresarUsuario`
        fetch(url,{
            method : 'POST',
            headers : {
                'Content-Type' : 'application/json',
                'Accept': 'application/json'
            },
            body : JSON.stringify(json),
        })
        .then(response=>response.json())
        .then(response => {
            if(response.codigo_usuario){
                GLOBAL.BASE_URL = localhost
                GLOBAL.currentUser = response
                Actions.home()
            } else {
                console.warn('Cellphone or Password incorrect')
            }
        })
        .catch(error => console.warn('Error:', error))
    }

    signin(localhost){
        Actions.signin({localhost})
    }

    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.mybuyStyle}>My Buy</Text>
                <TextInput style={styles.inputStyle} onChangeText={this.setCellphone} placeholder="Cellphone"/>
                <TextInput style={styles.inputStyle} secureTextEntry={true} onChangeText={this.setPassword} placeholder="Password"/>
                <TextInput style={styles.inputStyle} value={this.state.localhost} onChangeText={this.setLocalhost} placeholder="Localhost"/>
                <TouchableOpacity style={styles.touchableStyle} 
                    onPress={()=>this.login(this.state.numero_telefono,this.state.contrasenia,this.state.localhost)}
                >
                    <Text style={{color : '#fff'}}>Log In</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.touchableStyle} onPress={()=>this.signin(this.state.localhost)}>
                    <Text style={{color : '#fff'}}>Sign In</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mybuyStyle : {
        fontSize : 30,
        color : '#fff',
        marginBottom : 10
    },
    inputStyle : {
        height : 40,
        width : '100%',
        backgroundColor : '#FFFFFF',
        marginTop : 0,
        borderRadius : 10,
        padding : 5,
        marginBottom : 5
    },
    container : {
        flex : 1,
        alignItems : 'center',
        justifyContent : 'center',
        padding : 5,
        backgroundColor : '#BCA9F5'
    },
    touchableStyle : {
        height : 40,
        width : '100%',
        alignItems : 'center',
        justifyContent : 'center',
        backgroundColor : '#8258FA',
        borderRadius : 10,
        marginBottom : 5
    }
})