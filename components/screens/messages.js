import React, {
    Component
} from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    FlatList,
    TextInput,
    StyleSheet,
    Button
} from 'react-native'
import {
    GiftedChat
} from 'react-native-gifted-chat'
import GLOBAL,{
    BASE_URL,
    currentUser
} from '../Global'

import Icons from 'react-native-vector-icons/Ionicons'
import ModalCompra from '../modals/modalCompra'

export default class Messages extends Component {
    state = {
        messages: [],
        webSocket : null,
        modalVisible: false,
        buttonDisabled : false
    }
    constructor(props) {
        super(props)

        this.listarMensajes = this.listarMensajes.bind(this)
        this.sendMessage = this.sendMessage.bind(this)
        this.onMessage = this.onMessage.bind(this)
        this.setModalVisible = this.setModalVisible.bind(this)

        this.state.webSocket = new WebSocket(`ws://${BASE_URL}:8080/MyBuy/UsuarioSesion/${currentUser.codigo_usuario}`);
        this.state.webSocket.onopen = this.onOpen;
        this.state.webSocket.onmessage = this.onMessage

        GLOBAL.webSocket = this.state.webSocket
    }
    funcionesWebSocket(accion) {
        this.state.webSocket.send(accion)
    }

    componentDidMount() {
        this.listarMensajes()
    }

    onOpen(message) {
        console.log(message)
    }

    onMessage(data) {
        let msg = data.data
        if (msg == 'listarMensajes') {
            this.listarMensajes()
        } else if(msg == 'despuesDeCompra'){
            this.listarMensajes()
            this.setState({
                buttonDisabled : true
            })
        }
    }

    listarMensajes() {
        let json = {
            codigo_usuario: currentUser.codigo_usuario,
            codigo_usuario_2: this.props.codigo_usuario
        }
        const url = `http://${BASE_URL}:8080/MyBuy/rest/MyBuyRest/listarMensajes`
        fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(json),
            })
            .then(response => response.json())
            .then(response => {
                let messages = []
                response.forEach(val => {
                    let json = {
                        _id: val.codigo_detalle_chat + '',
                        text: val.mensaje,
                        createdAt: new Date(),
                        user: {
                            _id: val.codigo_usuario_1 + '',
                            name: val.usuario1,
                            avatar: `http://www.pokexperto.net/nds/artwork/${val.codigo_usuario_1}.jpg`,
                        }
                    }
                    messages.push(json)
                })
                this.setState({
                    messages
                })
            })
            .catch(error => console.warn('Error:', error))
    }

    sendMessage(message){
        let json = {
            codigo_usuario_1 : currentUser.codigo_usuario,
            codigo_usuario_2 : this.props.codigo_usuario,
            mensaje : message[0].text,
            tipo_mensaje : '0'
        }

        
        const url = `http://${BASE_URL}:8080/MyBuy/rest/MyBuyRest/registrarMensaje`
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(json),
        })
        .then(response => response.json())
        .then(response => {
            if(response == '1'){
                this.funcionesWebSocket('listarMensajes')
            } else {
                console.warn(response)
            }
            })
            .catch(error => console.warn('Error:', error))
        
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return ( 
            <View style={{flex : 1}}>

                <TouchableOpacity
                    style={{
                        backgroundColor : this.state.buttonDisabled ? '#47E353' : '#E11DAA',
                        height : 30,
                        width : 30,
                        borderRadius : 5,
                        marginRight : 10,
                        alignItems : 'center',
                        justifyContent : 'center',
                        position : 'absolute',
                        top : 10,
                        right : 10,
                        zIndex : 1
                    }}
                    onPress={() => {
                        if(this.state.buttonDisabled){
                        } else {
                            this.refs.modalCompra.showAddModal()                            
                        }
                    }}>

                    <Icons 
                        name={'ios-cart'}
                        size={20}
                        color={'#fff'}
                    />

                </TouchableOpacity>
                
                <GiftedChat messages = {
                    this.state.messages
                }
                onSend = {
                    (message) => {
                        this.sendMessage(message)
                    }
                }
                user = {
                    {
                        _id: currentUser.codigo_usuario + '',
                        name: currentUser.nickname
                    }
                }
                />
                <ModalCompra ref={'modalCompra'} ws={this.state.webSocket} codigo={this.props.codigo_usuario}></ModalCompra>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewForm: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        padding: 5,
        flexDirection: 'row'
    },
    inputText: {
        height: 30,
        width: '80%',
        backgroundColor: '#fff',
    }
})