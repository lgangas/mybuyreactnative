import React , {Component} from 'react'
import {Text, TouchableOpacity, TextInput, StyleSheet, View, FlatList} from 'react-native'
import {Actions} from 'react-native-router-flux'
import {currentUser,BASE_URL} from '../Global'

export default class MyBuys extends Component {

    constructor(props){
        super(props)
        
        this.state = {
            loading : false,
            data : [],            
        }

        this.buscarCompras = this.buscarCompras.bind(this)

    }

    componentDidMount(){
        this.buscarCompras(currentUser.codigo_usuario)
    }

    buscarCompras(codigo_usuario){

            let url = `http://${BASE_URL}:8080/MyBuy/rest/MyBuyRest/listarCompras`
    
            fetch(url,{
                method : 'POST',
                headers : {
                    'Content-Type' : 'application/json',
                    'Accept': 'application/json'
                },
                body : JSON.stringify({codigo_usuario : parseInt(codigo_usuario)}),
            })
                .then(response => response.json())
                .catch(error => console.error('Error:', error))
                .then(response => {
                    this.setState({
                        data : response
                    })
                })  

    }


    render(){
        return(
            <View style={styles.contatiner}>
                <FlatList style={styles.listContainerStyle}
                    //data ={this.state.data}
                    data={this.state.data}
                    renderItem={({item,index}) => (
                            <ListItem 
                                title={item.nickname}
                                subtitle={item.monto}
                                keyU1={item.codigo_usuario}
                                keyU2={item.codigo_usuario_2}
                            />
                        )
                    }
                    keyExtractor = {(item,index)=> item.codigo_usuario + ''}
                />
            </View> 
        )
    }
}

class ListItem extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <View style={{padding : 5,backgroundColor : '#fff'}}>
                <TouchableOpacity 
                    style={{width : '100%'}} 
                    onPress={()=>{
                        let codigoUsu = this.props.keyU1 == currentUser.codigo_usuario ? this.props.keyU2 : this.props.keyU1
                        Actions.recomendacion({codigoUsu})
                    }                        
                    } >                    
                    <Text style={{fontSize : 12}}>{this.props.title}</Text>
                    <Text style={{fontSize : 8}}>{this.props.subtitle}</Text>
                </TouchableOpacity>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    contatiner : {
        padding : 5,
        flex : 1,
        alignItems : "center",
        backgroundColor : '#E6E6E6'
    },
    inputStyle : {
        height : 40,
        width : '100%',
        backgroundColor : '#FFFFFF',
        marginTop : 0,
        borderRadius : 10,
        padding : 5
    },
    listContainerStyle : {
        width : '100%',
        height : '100%',
        marginTop : 10
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    }
})