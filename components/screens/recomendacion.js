import React,{Component} from 'react'
import {Text,View,TouchableOpacity,TextInput} from 'react-native'
import {BASE_URL,currentUser} from '../Global'
import { Actions } from 'react-native-router-flux';


export default class Recomendacion extends Component{

    constructor(props){
        super(props)
        this.state={
            comentario : '',
            puntaje : 0.0
        }
    }

    registrarRecomendacion(codigo_usuario,comentario,puntaje){
        let json = {
            codigo_usuario_2 : currentUser.codigo_usuario,
            codigo_usuario,
            comentario,
            puntaje
        }
        this.registrarComentario(json)
        .then(()=>{
            this.registrarPuntaje(json)
            .then(()=>Actions.pop())
        })
    }

    registrarComentario(json){
        return new Promise((resolve,reject)=>{
            let url = `http://${BASE_URL}:8080/MyBuy/rest/MyBuyRest/registrarComentario`
    
            fetch(url,{
                method : 'POST',
                headers : {
                    'Content-Type' : 'application/json',
                    'Accept': 'application/json'
                },
                body : JSON.stringify(json),
            })
                .then(response => response.json())
                .catch(error => console.error('Error:', error))
                .then(response => {
                    resolve()
                })  
        })
    }

    registrarPuntaje(json){
        return new Promise((resolve,reject)=>{
            let url = `http://${BASE_URL}:8080/MyBuy/rest/MyBuyRest/registrarPuntaje`
    
            fetch(url,{
                method : 'POST',
                headers : {
                    'Content-Type' : 'application/json',
                    'Accept': 'application/json'
                },
                body : JSON.stringify(json),
            })
                .then(response => response.json())
                .catch(error => console.error('Error:', error))
                .then(response => {
                    resolve()
                })  
        })
    }

    render(){
        return(
            <View style={{flex : 1,padding : 10, alignItems : 'center',justifyContent : 'center',backgroundColor : '#698DD0'}}>
                <TextInput 
                    onChangeText={texto=>this.state.comentario=texto}
                    style={{width : '100%',height : 40,margin : 15,backgroundColor : '#fff'}} placeholder='Comentario' />
                <TextInput 
                    onChangeText={texto=>this.state.puntaje=parseFloat(texto)}
                    style={{width : '100%',height : 40,margin : 15,backgroundColor : '#fff'}} placeholder='Puntaje'/>
                <TouchableOpacity 
                    onPress={()=>{
                        this.registrarRecomendacion(this.props.codigoUsu,this.state.comentario,this.state.puntaje)
                    }}
                    style={{alignItems : 'center',justifyContent:'center',backgroundColor:'#821277',height : 40,width : '100%',borderRadius : 5}}>
                    <Text style={{color : '#fff'}}>Aceptar</Text>    
                </TouchableOpacity>    
            </View>
        )
    }
}