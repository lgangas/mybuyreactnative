import React,{Component} from 'react'
import {StyleSheet,View, Text, TextInput, TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'
import { createBottomTabNavigator, createAppContainer } from 'react-navigation'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Searching from './searching'
import Profile from './profile'
import MyBuys from './mybuys'
const GLOBAL = require('../Global')

const HomeScreen = createBottomTabNavigator({
    MyBuys: {screen : MyBuys},
    Buscar: {screen : Searching},
    Perfil: {screen : Profile},
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'MyBuys') {
          iconName = `cart`;
        } else if (routeName === 'Buscar') {
          iconName = `search`;        
        } else if (routeName === 'Perfil') {
          iconName = `person`;
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Ionicons name={`ios-${iconName}`} size={horizontal ? 20 : 25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
    initialRouteName : 'Buscar'
  },
);
  
export default createAppContainer(HomeScreen)

const styles = StyleSheet.create({
    contatiner : {
        flex : 1,
        alignItems : 'center',
        justifyContent : 'center'
    }
})