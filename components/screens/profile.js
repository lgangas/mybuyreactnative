import React,{Component} from 'react'
import {View, Text, TouchableOpacity,FlatList,StyleSheet,Image,Modal,TextInput} from 'react-native'
import ModalComentario from '../modals/modalComentario'
import Ionicons from 'react-native-vector-icons/Ionicons'
import {BASE_URL} from '../Global'

export default class Profile extends Component {        
      
    constructor(props){
        super(props)

        this.state = {
            codigo_usuario : '',
            numero_telefono : '',
            nickname : '',
            categoria : '',
            descripcion : '',
            promedio_puntaje : '',
            cantidad_mensajes : '',
            modalVisible: false,
            dataComentario : []
        }
        this.cargarDataPerfil = this.cargarDataPerfil.bind(this)
        this.setModalVisible = this.setModalVisible.bind(this)
        this.listarComentarios = this.listarComentarios.bind(this)
    }

    listarComentarios(codigo_usuario){
        
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    componentDidMount(){
        this.cargarDataPerfil()
    }

    cargarDataPerfil(){
        if(this.props.codigo_usuario){
            let codigo_usuario = this.props.codigo_usuario
            let url = `http://${BASE_URL}:8080/MyBuy/rest/MyBuyRest/datosPerfil`
            fetch(url,{
                method : 'POST',
                headers : {
                    'Content-Type' : 'application/json',
                    'Accept': 'application/json'
                },
                body : JSON.stringify({codigo_usuario}),
            })
            .then(response=>response.json())
            .then(response => {
                this.setState({
                    codigo_usuario : response.codigo_usuario,
                    numero_telefono : response.numero_telefono,
                    nickname : response.nickname,
                    categoria : response.categoria,
                    descripcion : response.descripcion,
                    promedio_puntaje : response.promedio_puntaje,
                    cantidad_mensajes : response.cantidad_mensajes
                })
            })
            .catch(error => console.warn('Error:', error))
        }
    }

    render(){
        return(
            <View style={styles.contatiner}>
                <Image 
                    style={{height : 100, width : '100%'}}
                    source={{uri : this.props.codigo_usuario ? 
                    `http://www.pokexperto.net/nds/artwork/${this.props.codigo_usuario}.jpg` : 
                    'https://www.rockandpop.cl/wp-content/uploads/2017/06/Thom-Yorke.jpg'}}/>
                <Text style={styles.nicknameStyle}>{this.state.nickname}</Text>
                <Text style={styles.textStyle}>{this.state.numero_telefono}</Text>
                <Text style={styles.textStyle}>Categoria: {this.state.categoria}</Text>
                <Text style={styles.textStyle}>Descripcion: {this.state.descripcion}</Text>
                <View style={styles.rowStyle}> 
                    <Text style={styles.textStyle}>Calificacion: {this.state.promedio_puntaje}</Text>
                </View>
                <View style={styles.rowStyle}> 
                    <Text style={styles.textStyle}>Comentarios: {this.state.cantidad_mensajes}</Text>
                    <TouchableOpacity  
                        style={styles.btnStyle}
                            onPress={() => {
                            this.refs.miModal.showAddModal();
                        }}>
                        <Ionicons name={'ios-eye'} size={15} color='#fff' />
                    </TouchableOpacity>
                </View>

                <ModalComentario ref={'miModal'} codigo={this.props.codigo_usuario}>

                </ModalComentario>

            </View>
        )
    }
}





const styles = StyleSheet.create({
    contatiner : {
        flex : 1,
        padding : 10,
    },
    containerModal : {
        flexDirection: 'row',
    },
    listContainerStyle : {
        width : '100%',
        height : '100%',
        marginTop : 10
    },
    nicknameStyle : {
        fontSize : 20,
        fontStyle : 'normal',
        marginBottom : 10,    
    },
    textStyle : {
        fontSize : 12,
        // width : '100%',        
        marginBottom : 10,
    },
    btnStyle : {
        borderRadius : 20,
        backgroundColor : '#08298A',
        width :15,
        height : 15,
        marginRight : 0,
        justifyContent : 'center',
        alignItems : 'center',
    },
    rowStyle : {
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})