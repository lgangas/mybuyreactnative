import React , {Component} from 'react'
import {Text, TouchableOpacity, TextInput, StyleSheet, View, FlatList} from 'react-native'
import {Actions} from 'react-native-router-flux'
const GLOBAL = require('../Global')

export default class Searching extends Component {

    constructor(props){
        super(props)
        
        this.state = {
            loading : false,
            data : [],            
        }

        this.buscarVendedor = this.buscarVendedor.bind(this)
    }

    buscarVendedor(numero_telefono){
        if(numero_telefono.length >= 3){

            let url = `http://${GLOBAL.BASE_URL}:8080/MyBuy/rest/MyBuyRest/buscarTelefono`
    
            fetch(url,{
                method : 'POST',
                headers : {
                    'Content-Type' : 'application/json',
                    'Accept': 'application/json'
                },
                body : JSON.stringify({numero_telefono}),
            })
                .then(response => response.json())
                .catch(error => console.error('Error:', error))
                .then(response => {
                    this.setState({
                        data : response
                    })
                })        
        }

    }


    render(){
        return(
            <View style={styles.contatiner}>
                <TextInput style={styles.inputStyle} onChangeText={this.buscarVendedor} />
                <FlatList style={styles.listContainerStyle}
                    //data ={this.state.data}
                    data={this.state.data}
                    renderItem={({item,index}) => (
                            <ListItem 
                                title={item.nickname}
                                subtitle={item.numero_telefono}
                                keyItem={item.codigo_usuario}
                            />
                        )
                    }
                    keyExtractor = {(item,index)=> item.codigo_usuario + ''}
                />
            </View> 
        )
    }
}

class ListItem extends Component{
    constructor(props){
        super(props)
    }

    verPerfil(codigo_usuario){
        Actions.profile({codigo_usuario})
    }

    verMensajes(codigo_usuario){
        Actions.messages({codigo_usuario})
    }

    render(){
        return(
            <View style={{padding : 5,backgroundColor : '#fff'}}>
                <TouchableOpacity 
                    style={{width : '100%'}} 
                    onPress={()=>this.verPerfil(this.props.keyItem)}
                    onLongPress={()=>this.verMensajes(this.props.keyItem)} >                    
                    <Text style={{fontSize : 12}}>{this.props.title}</Text>
                    <Text style={{fontSize : 8}}>{this.props.subtitle}</Text>
                </TouchableOpacity>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    contatiner : {
        padding : 5,
        flex : 1,
        alignItems : "center",
        backgroundColor : '#E6E6E6'
    },
    inputStyle : {
        height : 40,
        width : '100%',
        backgroundColor : '#FFFFFF',
        marginTop : 0,
        borderRadius : 10,
        padding : 5
    },
    listContainerStyle : {
        width : '100%',
        height : '100%',
        marginTop : 10
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    }
})