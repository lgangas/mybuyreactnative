import React,{Component} from 'react'
import {StyleSheet,View, Text, TextInput, TouchableOpacity,Alert} from 'react-native'
import {Actions} from 'react-native-router-flux'

export default class Signin extends Component {

    constructor(props){
        super(props)
        this.state = {
            numero_telefono : '',
            contrasenia : '',
            nickname : '',
            categoria : '',
            numero_cuenta_bancaria : '',
            descripcion : '',
            localhost : props.navigation.state.params.localhost
        }
        this.registrate = this.registrate.bind(this)
    }

    registrate(json){
        let url = `http://${json.localhost}:8080/MyBuy/rest/MyBuyRest/registrarUsuario`

        delete json['localhost']

        fetch(url,{
            method : 'POST',
            headers : {
                'Content-Type' : 'application/json',
                'Accept': 'application/json'
            },
            body : JSON.stringify(json),
        })
        .then(response=>response.json())
        .then(response => {
            if(response == 1){
                Alert.alert(
                    'Success',
                    'Welcome to MyBuy!!',
                    [
                        {text: 'Accept', onPress: () => Actions.pop()}
                    ]
                )
            } else {
                console.warn('Error inesperado.')
            }
        })
        .catch(error => console.warn('Error:', error))
    }

    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.titleStyle}>Sign In</Text>
                <TextInput 
                    style={styles.inputStyle} 
                    onChangeText={(texto)=>{this.state.numero_telefono = texto}} 
                    placeholder="Cellphone"/>
                <TextInput 
                    style={styles.inputStyle} 
                    onChangeText={(texto)=>{this.state.contrasenia = texto}}
                    placeholder="Password"/>
                <TextInput 
                    style={styles.inputStyle} 
                    onChangeText={(texto)=>{this.state.nickname = texto}} 
                    placeholder="Nickname"/>
                <TextInput 
                    style={styles.inputStyle} 
                    onChangeText={(texto)=>{this.state.categoria = texto}} 
                    placeholder="Category"/>
                <TextInput 
                    style={styles.inputStyle}
                    onChangeText={(texto)=>{this.state.numero_cuenta_bancaria = texto}}
                    placeholder="Bank Account"/>
                <TextInput 
                    style={styles.inputStyle} 
                    onChangeText={(texto)=>{this.state.descripcion = texto}} 
                    placeholder="Description"/>
                <TouchableOpacity style={styles.touchableStyle} onPress={()=>this.registrate(this.state)}>
                    <Text style={{color : '#fff'}}>Sign In</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    titleStyle : {
        fontSize : 30,
        color : '#fff',
        marginBottom : 20,
    },
    inputStyle : {
        height : 40,
        width : '100%',
        backgroundColor : '#FFFFFF',
        marginTop : 0,
        borderRadius : 10,
        padding : 5,
        marginBottom : 5
    },
    container : {
        flex : 1,
        alignItems : 'center',
        padding : 5,
        backgroundColor : '#BCA9F5'
    },
    touchableStyle : {
        height : 40,
        width : '100%',
        alignItems : 'center',
        justifyContent : 'center',
        backgroundColor : '#8258FA',
        borderRadius : 10,
        marginBottom : 5
    }
})