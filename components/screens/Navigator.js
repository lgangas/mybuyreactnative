import React,{Component} from 'react'
import {Router,Stack,Scene} from 'react-native-router-flux'
import Login from './login'
import TabComponents from './tabComponents'
import Signin from './signin'
import Profile from './profile'
import Messages from './messages'
import Recomendacion from './recomendacion';

export default class Navigator extends Component {
    render(){
        return(
            <Router>
                <Stack key="root1" >
                    <Scene key="login" hideNavBar component={Login} title="Login" initial/>
                    <Scene key="home" hideNavBar component={Home} title="Home"/>
                    <Scene key="signin" component={Signin} title="Signin"/>
                    <Scene key="profile" hideNavBar component={Profile} title="Profile"/>
                    <Scene key="messages" hideNavBar component={Messages} title="Messages"/>
                    <Scene key="recomendacion" hideNavBar component={Recomendacion} title="Recomendacion"/>
                </Stack>
            </Router>
        )
    }
}

class Home extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <TabComponents />
        )
    }
}