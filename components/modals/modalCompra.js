import React,{Component} from 'react'
import {FlatList,StyleSheet,Text,View,Platform,TouchableOpacity,Dimensions,TextInput} from 'react-native'
import Modal from 'react-native-modalbox'
import Button from 'react-native-button'
import {BASE_URL,currentUser} from '../Global'

var screen = Dimensions.get('window')

export default class ModalCompra extends Component {
    constructor(props){
        super(props)
        this.state={
            monto : 0.0
        }
    }

    showAddModal(){
        this.refs.miModal.open()
    }

    realizarCompra(codigo_usuario,monto,webSocket){
        let json = {
            codigo_usuario_1 : currentUser.codigo_usuario,
            codigo_usuario_2 : codigo_usuario + '',
            monto : parseFloat(monto),
            mensaje : `Se desea realizar una compra por el monto de ${monto}.`,
            tipo_mensaje : '1'
        }

        const url = `http://${BASE_URL}:8080/MyBuy/rest/MyBuyRest/registrarCompra`
        fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(json),
            })
            .then(response => response.json())
            .then(response => {
                if(response == 1){
                    this.refs.miModal.close()
                    webSocket.send('listarMensajes')
                } else {
                    alert('Error inesperado.')
                }
            })
            .catch(error => console.warn('Error:', error))

    }

    render(){
        return(
            <Modal
                ref={'miModal'}
                style={styles.modalView}
                position='center'
                backdrop={true}
                onClosed={()=>{}}
                swipeToClose={false}
            >

                <View style={styles.container}>
                    <Text style={{fontSize : 20,marginBottom : 10}}>Realizar compra</Text>
                    <TextInput 
                        style={{
                            backgroundColor : '#C7D4D4',
                            width : '100%',
                            height : 40,                              
                        }} 
                        onChangeText={texto => this.state.monto = texto}
                        placeholder='Monto acordado' />
                </View> 

                <TouchableOpacity
                    style={styles.acceptButtom}
                    onPress={()=>this.realizarCompra(this.props.codigo,this.state.monto,this.props.ws)}
                >
                    <Text style={{color : '#fff'}}>Aceptar</Text>
                </TouchableOpacity>
                    
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    acceptButtom : {
        alignItems : 'center',
        justifyContent : 'center',
        width : '100%',
        backgroundColor : '#4FD45A',
        position: 'absolute', 
        left: 0, 
        right: 0, 
        bottom: 0,
        padding : 5
    },
    modalView : {
        justifyContent : 'center',
        width : screen.width - 80,
        height : 200
    },
    container : {
        flex : 1,
        padding : 10,
        alignItems :'center',
        justifyContent : 'center'
    },
    containerModal : {
        flexDirection: 'row',
    },
    listContainerStyle : {
        width : '100%',
        height : '100%',
        marginTop : 10
    }
})