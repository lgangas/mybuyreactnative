import React,{Component} from 'react'
import {FlatList,StyleSheet,Text,View,Platform,TouchableOpacity,Dimensions,TextInput} from 'react-native'
import Modal from 'react-native-modalbox'
import Button from 'react-native-button'
import {BASE_URL} from '../Global'

var screen = Dimensions.get('window')

export default class ModalComentario extends Component {
    constructor(props){
        super(props)
        this.state={
            dataComentario : []
        }

        this.getData = this.getData.bind(this)
    }

    showAddModal(){
        this.refs.miModal.open()
    }

    getData(){
        let codigo_usuario = this.props.codigo   

        const url = `http://${BASE_URL}:8080/MyBuy/rest/MyBuyRest/listarComentarios`
        fetch(url,{
            method : 'POST',
            headers : {
                'Content-Type' : 'application/json',
                'Accept': 'application/json'
            },
            body : JSON.stringify({codigo_usuario}),
        })
        .then(response=>response.json())
        .then(response => {
            this.setState({
                dataComentario : response
            })
        })
        .catch(error => console.warn('Error:', error))
    }

    render(){
        return(
            <Modal
                ref={'miModal'}
                style={styles.modalView}
                position='center'
                backdrop={true}
                onClosed={()=>{}}
                onOpened={this.getData}
                swipeToClose={false}
            >

                <View style={styles.container}>
                    <FlatList
                    style={styles.listContainerStyle}
                    data={this.state.dataComentario}
                    renderItem={({item,index}) => (
                        <ListItem 
                            title={item.nickname}
                            subtitle={item.comentario}
                        />
                    )}
                    keyExtractor = {(item,index)=> index + ''}
                />
                </View>

                <TouchableOpacity
                    style={styles.closeButtom}
                    onPress={()=>this.refs.miModal.close()}
                >
                    <Text style={{color : '#fff'}}>close</Text>
                </TouchableOpacity>
                    
            </Modal>
        )
    }
}

class ListItem extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <View style={{padding : 5,borderBottomColor : '#A4A4A4'}}>
                <Text style={{fontSize : 12}}>{this.props.title}</Text>
                <Text style={{fontSize : 8}}>{this.props.subtitle}</Text>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    closeButtom : {
        alignItems : 'center',
        justifyContent : 'center',
        width : '100%',
        backgroundColor : '#F56E6E',
        position: 'absolute', 
        left: 0, 
        right: 0, 
        bottom: 0,
        padding : 5
    },
    modalView : {
        justifyContent : 'center',
        width : screen.width - 40,
        height : screen.height - 40,
        marginBottom : 40,
        flex : 1   
    },
    container : {
        flex : 1,
        padding : 10,
    },
    containerModal : {
        flexDirection: 'row',
    },
    listContainerStyle : {
        width : '100%',
        height : '100%',
        marginTop : 10
    }
})